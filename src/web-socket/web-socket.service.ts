import {OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, WebSocketGateway} from "@nestjs/websockets";
import {Server, Socket} from "socket.io";
import {Logger} from "@nestjs/common";

@WebSocketGateway({transports: ['websocket']})
export class WebSocketService implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    private server: Server;
    private clientMap: Map<string, Socket>;
    private logger: Logger = new Logger('WebSocketGateway');

    afterInit(server: any): any {
        this.server = server;
        this.clientMap = new Map<string, Socket>();
        this.logger.log('WebSocketGateway start');
    }

    handleConnection(client: any, ...args: any[]): any {
    }

    handleDisconnect(client: any): any {
    }

    emitToSingle(userId: string, topic: string, content: string): void {
        this.clientMap.get(userId)?.emit(topic, content);
    }

    emitToAll(topic: string, content: string): void {
        this.server.emit(topic, content);
    }
}


