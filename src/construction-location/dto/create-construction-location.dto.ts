import {ApiProperty} from "@nestjs/swagger";

export class CreateConstructionLocationDto {
    @ApiProperty()
    name: string;
}