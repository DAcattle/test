import {ApiProperty} from "@nestjs/swagger";

export class ConstructionLocation {
    @ApiProperty()
    id: string;

    @ApiProperty()
    name: string;
}