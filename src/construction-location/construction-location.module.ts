import {Module} from '@nestjs/common';
import {ConstructionLocationService} from './construction-location.service';
import {ConstructionLocationController} from './construction-location.controller';

@Module({
  providers: [ConstructionLocationService],
  controllers: [ConstructionLocationController]
})
export class ConstructionLocationModule {
}
