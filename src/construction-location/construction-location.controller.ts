import {Body, Controller, Post} from '@nestjs/common';
import {CreateConstructionLocationDto} from "./dto/create-construction-location.dto";
import {ApiTags} from "@nestjs/swagger";

@Controller('construction-location')
@ApiTags('construction-location')
export class ConstructionLocationController {
    @Post()
    createConstructionLocation(@Body() dto: CreateConstructionLocationDto) {

    }
}
