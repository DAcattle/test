import {Injectable} from '@nestjs/common';
import {Neo4jService} from "../neo4j/neo4j.service";
import {tap} from "rxjs";

const seedData = {
    notifyInterval: 5,
    notifyRecordTimes: 5
}

@Injectable()
export class SystemSettingService {
    constructor(
        private readonly neo4jService: Neo4jService
    ) {
        // this.getSystemSetting().then(
        //     value => {
        //         if (!value) this.neo4jService.write(`CREATE (s:SystemSetting $seed)`, {seed: seedData});
        //     });
    }

    async updateSystemSetting(param) {
        const result = await this.neo4jService.write(
            'MATCH (s:SystemSetting) ' +
            'SET s +=  ' +
            'RETURN s',
            param
        );

        const record = result.records[0].get('s');
    }

    getSystemSetting() {
        this.neo4jService.read('MATCH (s:SystemSetting) RETURN s')
            .records()
            .pipe(
                tap((x: any) => console.log(x))
            )
        // .subscribe(()=>{})
    }

    // {
    // onNext: record => {
    //     console.log(record.get('s').properties);
    //     return record.get('s').properties;
    // }

    // const record = result.records[0]?.get('s') ?? null;
    // return record.properties;
}
