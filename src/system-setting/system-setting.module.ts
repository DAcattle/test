import {Module} from '@nestjs/common';
import {SystemSettingService} from './system-setting.service';
import {SystemSettingController} from './system-setting.controller';

@Module({
  providers: [SystemSettingService],
  controllers: [SystemSettingController]
})
export class SystemSettingModule {
}
