import {Controller, Get} from '@nestjs/common';
import {SystemSettingService} from "./system-setting.service";
import {ApiTags} from "@nestjs/swagger";

@Controller('system-setting')
@ApiTags('system-setting')
export class SystemSettingController {
    constructor(private readonly service: SystemSettingService) {
    }

    @Get()
    getSystemSetting() {
        return this.service.getSystemSetting();
    }
}
