import {Test, TestingModule} from '@nestjs/testing';
import {AppController} from "./app.controller";
import {AppService} from "./app.service";
import * as request from 'supertest';
import {INestApplication} from "@nestjs/common";
import {AppModule} from "./app.module";

describe('TtController', () => {
    let app: INestApplication;
    let controller: AppController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
            controllers: [AppController],
            providers: [AppService]
        }).compile();

        controller = module.get<AppController>(AppController);
        app = module.createNestApplication();
    });

    it('should be defined', () => {
        expect(controller.getHello()).toBe('Hello World!');
    });

    it('authentication', async () => {
        let res = await request(app.getHttpServer())
            .get('/auth').expect(200);
    });
});
