import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {AuthModule} from './auth/auth.module';
import {UserModule} from './user/user.module';
import {ConfigModule} from "@nestjs/config";
import {FcmModule} from './fcm/fcm.module';
import {FcmTokenModule} from './fcm-token/fcm-token.module';
import {WebSocketModule} from './web-socket/web-socket.module';
import {ConstructionLocationModule} from './construction-location/construction-location.module';
import {CameraModule} from './camera/camera.module';
import {SystemSettingModule} from './system-setting/system-setting.module';
import {NotificationModule} from './notification/notification.module';
import {AbnormalModule} from './abnormal/abnormal.module';
import {IncidentRecordModule} from './incident-record/incident-record.module';
import {Neo4jModule} from './neo4j/neo4j.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: process.env.NODE_ENV === 'production' ? ['.production.env', '.development.env'] : '.development.env',
            isGlobal: true
        }),
        Neo4jModule.forRoot({
            scheme: 'neo4j',
            host: 'localhost',
            port: 7687,
            username: 'neo4j',
            password: '1234',
        }),
        AuthModule,
        UserModule,
        FcmModule,
        FcmTokenModule,
        WebSocketModule,
        ConstructionLocationModule,
        CameraModule,
        SystemSettingModule,
        NotificationModule,
        AbnormalModule,
        IncidentRecordModule,
        Neo4jModule,
    ],
    controllers: [AppController],
    providers: [AppService,
        // {provide: APP_GUARD, useClass: JwtGuard}
    ],
})
export class AppModule {
}
