import {Controller, Get, Request} from '@nestjs/common';
import {AppService} from './app.service';
import {Public} from "./common/decorator/public.decorator";
import {ApiTags} from "@nestjs/swagger";

@ApiTags('app')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {
  }

  @Get()
  @Public()
  getHello() {
    return this.appService.getHello();
  }

  @Get('/auth')
  authenticationTest(@Request() req) {
    return 'validate!';
  }
}
