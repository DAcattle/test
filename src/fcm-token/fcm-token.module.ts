import {Module} from '@nestjs/common';
import {FcmTokenService} from './fcm-token.service';
import {FcmTokenController} from './fcm-token.controller';

@Module({
  providers: [FcmTokenService],
  controllers: [FcmTokenController]
})
export class FcmTokenModule {
}
