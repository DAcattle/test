export type Neo4jScheme = 'neo4j' | 'neo4j+s' | 'neo4j+scc' | 'bolt' | 'bolt+s' | 'bolt+scc'

export interface Neo4jConfig {
    scheme: Neo4jScheme;
    host: string;
    port: number | string;
    username: string;
    password: string;
    database?: string;
}

export const NEO4J_OPTIONS = 'NEO4J_OPTIONS'
export const NEO4J_DRIVER = 'NEO4J_DRIVER'
