import {Neo4jConfig} from "./neo4j.config";

export const createDriver = async (config: Neo4jConfig) => {
    const neo4j = require('neo4j-driver')
    const driver = neo4j.driver(
        `${config.scheme}://${config.host}:${config.port}`,
        neo4j.auth.basic(config.username, config.password),
    );

    await driver.verifyConnectivity();

    return driver;
}