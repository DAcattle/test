import {Inject, Injectable} from '@nestjs/common';
import {NEO4J_DRIVER, NEO4J_OPTIONS, Neo4jConfig} from "./neo4j.config";
import neo4j, {Driver} from "neo4j-driver";

@Injectable()
export class Neo4jService {
    private readonly config: Neo4jConfig;
    private readonly driver: Driver;

    constructor(
        @Inject(NEO4J_OPTIONS) config: Neo4jConfig,
        @Inject(NEO4J_DRIVER) driver: Driver,
    ) {
        this.config = config;
        this.driver = driver;
    }

    getWriteRxSession(database?: string) {
        return this.driver.rxSession({
            database: database || this.config.database,
            defaultAccessMode: neo4j.session.WRITE
        });
    }

    getReadRxSession(database?: string) {
        return this.driver.rxSession({
            database: database || this.config.database,
            defaultAccessMode: neo4j.session.READ
        });
    }

    write(cypher: string, params?: Record<string, any>, database?: string) {
        const session = this.getWriteRxSession(database);
        return session.run(cypher, params);
    }

    read(cypher: string, params?: Record<string, any>, database?: string) {
        const session = this.getReadRxSession(database);
        return session.run(cypher, params);
    }
}
