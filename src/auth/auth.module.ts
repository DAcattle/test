import {Module} from '@nestjs/common';
import {AuthService} from './auth.service';
import {PassportModule} from "@nestjs/passport";
import {LocalStrategy} from "./startegies/local.strategy";
import {UserModule} from "../user/user.module";
import {JwtModule} from "@nestjs/jwt";
import {JwtStrategy} from "./startegies/jwt.strategy";
import {AuthController} from './auth.controller';
import {JwtRefreshStrategy} from "./startegies/jwt-refresh.strategy";

const fs = require('fs');

@Module({
    imports: [
        UserModule,
        PassportModule,
        JwtModule.register({
            privateKey: fs.readFileSync('./at.key'),
            publicKey: fs.readFileSync('./at.key.pub'),
            signOptions: {expiresIn: 60, algorithm: 'RS256'},
        }),
        // .registerAsync({
        // useFactory: async (configService: ConfigService) => ({
        //     privateKey: fs.readFileSync('./at.key'),
        //     publicKey: fs.readFileSync('./at.key.pub'),
        //     signOptions: {expiresIn: '60s', algorithm: 'RS256'},
        // }),
        //     inject: [ConfigService],
        // }),
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy, JwtRefreshStrategy],
    exports: [AuthService],
    controllers: [AuthController]
})
export class AuthModule {
}
