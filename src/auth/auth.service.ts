import {ForbiddenException, Injectable} from '@nestjs/common';
import {UserService} from "../user/user.service";
import {JwtService} from "@nestjs/jwt";
import {ConfigService} from "@nestjs/config";
import * as fs from "fs";

const bcrypt = require('bcrypt');

@Injectable()
export class AuthService {
    constructor(
        private config: ConfigService,
        private readonly userService: UserService,
        private jwtService: JwtService,
    ) {
    }

    async login(user: any) {
        const tokens = this.getTokens(user);
        await this.updateRefreshToken(user.id, tokens.refresh_token);
        return tokens;
    }

    async logout(userId: string) {

    }

    async refreshToken(userId: string, refreshToken: string) {
        const user = await this.userService.users.find(user => user.id === userId);
        if (!user) throw new ForbiddenException("Access Denied");

        console.log('refresh: ', refreshToken);
        console.log('user refresh: ', user.refreshToken);
        const isMatch = await bcrypt.compare(refreshToken, user.refreshToken);
        console.log(isMatch);
        if (!isMatch) throw new ForbiddenException("Access Denied");

        const tokens = this.getTokens(user);
        await this.updateRefreshToken(user.id, tokens.refresh_token);
        return tokens;
    }

    async validateUser(username: string, password: string): Promise<any> {
        const user = await this.userService.findOne(username);
        if (user && user.password === password) {
            const {password, ...result} = user;
            return result;
        }
        return null;
    }

    getTokens(user: any) {
        const payload = {username: user.username, sub: user.id};
        return {
            access_token: this.jwtService.sign(payload),
            refresh_token: this.jwtService.sign(payload, {privateKey: fs.readFileSync('./rt.key'), expiresIn: 60 * 60 * 24 * 7, algorithm: 'RS256'}),
        };
    }

    async hashData(data: string) {
        return await bcrypt.hash(data, 10);
    }

    async updateRefreshToken(userId: string, refreshToken: string) {
        const hash = await this.hashData(refreshToken);
        await this.userService.updateRefreshToken(userId, hash);
    }
}
