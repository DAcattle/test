import {Controller, HttpCode, HttpStatus, Post, Request, UseGuards} from '@nestjs/common';
import {LocalGuard} from "./guards/local.guard";
import {AuthService} from "./auth.service";
import {JwtRefreshGuard} from "./guards/jwt-refresh.guard";
import {Public} from "../common/decorator/public.decorator";
import {ApiParam, ApiTags} from "@nestjs/swagger";

@ApiTags('auth')
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {
    }

    @Post('/login')
    @Public()
    @UseGuards(LocalGuard)
    @ApiParam({name: 'username'})
    @ApiParam({name: 'password'})
    async login(@Request() req) {
        const user = req.user;
        return this.authService.login(user);
    }

    @Post('/logout')
    @HttpCode(HttpStatus.OK)
    async logout(@Request() req) {
        const user = req.user;
        return this.authService.logout(user['sub']);
    }

    @UseGuards(JwtRefreshGuard)
    @Post('/refresh')
    @HttpCode(HttpStatus.OK)
    refreshToken(@Request() req) {
        const user = req.user;
        return this.authService.refreshToken(user['sub'], user['refreshToken']);
    }
}
