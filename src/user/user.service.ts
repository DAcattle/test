import {Injectable} from '@nestjs/common';
import {User} from "./user.entity";

@Injectable()
export class UserService {
    users = [
        new User('1', 'user1', '111'),
        new User('2', 'user2', '222')
    ]

    async findOne(username: string): Promise<User | undefined> {
        return this.users.find(user => user.username === username);
    }

    async updateRefreshToken(userId: string, refreshToken: string) {
        const user = this.users.find(user => user.id === userId);
        user.refreshToken = refreshToken;
        return user;
    }
}
