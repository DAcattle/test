import {Body, Controller, Get, HttpStatus, Post, Put} from '@nestjs/common';
import {ApiBody, ApiResponse, ApiTags} from "@nestjs/swagger";
import {Camera} from "./camera.entity";
import {UpdateCameraDto} from "./dto/update-camera.dto";
import {CreateCameraDto} from "./dto/create-camera.dto";

@Controller('camera')
@ApiTags('camera')
export class CameraController {
    @Post()
    @ApiBody({type: CreateCameraDto, isArray: true})
    @ApiResponse({status: HttpStatus.CREATED, type: Camera, isArray: true})
    createCameras(@Body() dto: CreateCameraDto[]) {

    }

    @Get()
    @ApiResponse({status: HttpStatus.OK, type: Camera, isArray: true})
    getCameras() {

    }

    @Put()
    @ApiBody({type: UpdateCameraDto, isArray: true})
    @ApiResponse({status: HttpStatus.OK, type: Camera, isArray: true})
    updateCameras(@Body() dto: UpdateCameraDto[]) {
    }
}
