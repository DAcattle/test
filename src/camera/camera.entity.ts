import {ApiProperty} from "@nestjs/swagger";

export class Camera {
    @ApiProperty()
    id: string;

    @ApiProperty()
    locationId: string;

    @ApiProperty()
    distributorId: string;

    @ApiProperty()
    name: string;

    @ApiProperty()
    url: string;

    @ApiProperty()
    state: CameraState;

    @ApiProperty()
    description: string;
}

export enum CameraState {
    ON,
    OFF
}