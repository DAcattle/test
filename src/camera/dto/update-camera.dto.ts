import {ApiProperty} from "@nestjs/swagger";
import {CameraState} from "../camera.entity";

export class UpdateCameraDto {
    @ApiProperty()
    id: string;

    @ApiProperty({required: false})
    locationId: string;

    @ApiProperty({required: false})
    distributorId: string;

    @ApiProperty({required: false})
    name: string;

    @ApiProperty({required: false})
    url: string;

    @ApiProperty({required: false})
    state: CameraState;

    @ApiProperty({required: false})
    description: string;
}