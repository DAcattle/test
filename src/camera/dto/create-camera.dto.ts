import {ApiProperty} from "@nestjs/swagger";
import {CameraState} from "../camera.entity";

export class CreateCameraDto {
    @ApiProperty()
    locationId: string;

    @ApiProperty()
    distributorId: string;

    @ApiProperty()
    name: string;

    @ApiProperty()
    url: string;

    @ApiProperty()
    state: CameraState;

    @ApiProperty()
    description: string;
}