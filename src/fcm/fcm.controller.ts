import {Controller, HttpCode, HttpStatus, Post, Request} from '@nestjs/common';
import {FcmService} from "./fcm.service";
import {Public} from "../common/decorator/public.decorator";
import {ApiTags} from "@nestjs/swagger";

@Controller('fcm')
@Public()
@ApiTags('fcm')
export class FcmController {
    constructor(private fcmService: FcmService) {
    }

    @Post('/subscribe')
    @HttpCode(HttpStatus.OK)
    subscribeTopic(@Request() request) {
        let body = request['body'];
        this.fcmService.subscribeTopics(body['tokens']);
    }

    @Post('/unsubscribe')
    @HttpCode(HttpStatus.OK)
    unsubscribeTopic(@Request() request) {
        let body = request['body'];
        this.fcmService.unsubscribeTopics(body['tokens']);
    }

    @Post('/topic')
    @HttpCode(HttpStatus.OK)
    sendByTopic(@Request() request) {
        let body = request['body'];
        this.fcmService.sendByTopic(body['title'], body['body'], body['topic']);
    }

    @Post('/tokens')
    @HttpCode(HttpStatus.OK)
    sendByTokens(@Request() request) {
        let body = request['body'];
        this.fcmService.sendByTokens(body['title'], body['body'], body['tokens']);
    }
}
