import {Injectable} from '@nestjs/common';
import {resolve} from "path";
import * as admin from 'firebase-admin';

export const topics = {
    TEST: 'test'
}

@Injectable()
export class FcmService {
    constructor() {
        const key = require(resolve('./service-accountK-key.json'));
        admin.initializeApp({
            credential: admin.credential.cert(key)
        });
    }

    subscribeTopics(tokens: string[]) {
        admin.messaging().subscribeToTopic(tokens, topics.TEST)
            .then((response) => console.log('Successfully subscribed to topic: ', response))
            .catch((error) => console.log('Error subscribed to topic: ', error));
    }

    unsubscribeTopics(tokens: string[]) {
        admin.messaging().unsubscribeFromTopic(tokens, topics.TEST)
            .then((response) => console.log('Successfully unsubscribed from topic:', response))
            .catch((error) => console.log('Error unsubscribing from topic:', error));
    }

    sendByTopic(title: string, body: string, topic: string) {
        const message = {
            notification: {
                title: title,
                body: body,
            },
            topic: topic
        };

        admin.messaging().send(message)
            .then((response) => console.log('Successfully sent message: ', response))
            .catch((error) => console.log('Error sending message: ', error));
    }

    sendByTokens(title: string, body: string, tokens: string[]) {
        const message = {
            notification: {
                title: title,
                body: body,
            },
            tokens: tokens,
        };

        admin.messaging().sendMulticast(message)
            .then((response) => {
                if (response.failureCount > 0) {
                    const failedTokens = [];
                    response.responses.forEach((resp, idx) => {
                        if (!resp.success) {
                            failedTokens.push(tokens[idx]);
                        }
                    });
                    console.log('List of tokens that caused failures: ' + failedTokens);
                }
            });
    }
}
